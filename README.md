# KOTR Indeo Codec Batch
Batch File for activating the Indeo 5 Video Codec before starting King of the Road / Hard Truck 2 and deactivating the Indeo 5 Video Codec after King of the Road / Hard Truck 2 is closed.
  
Batch file will ask for admin rights (needed for registering the codec).
  
If codec activated successfully, popup window should confirm that codec is registered.  
If codec deactivated successfully, popup window with runtime error should appear (but codec will be deactivated anyway).
  
Tested on Windows 10 (x64). Should work on x86 too (but untested).

# How to use
Just put the START_KOTR(.bat) file in the King of the Road / Hard Truck 2 install folder where the king.exe file is and doubleclick the START_KOTR(.bat) file.