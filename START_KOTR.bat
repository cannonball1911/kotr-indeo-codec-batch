::Batch File for activating the Indeo 5 Video Codec befor starting King of the Road / Hard Truck 2
::and deactivating the Indeo 5 Video Codec after King of the Road / Hard Truck 2 is closed.
::Written by Cannonball1911 - https://gitlab.com/cannonball1911
::2019/01/18

@echo off
setlocal
title %~nx0

:: BatchGotAdmin (Run as Admin code starts)
REM  --> Check for permissions
>nul 2>&1 "%SYSTEMROOT%\system32\cacls.exe" "%SYSTEMROOT%\system32\config\system"

REM --> If error flag set, we do not have admin.
if '%errorlevel%' NEQ '0' (
    echo Requesting administrative privileges...
    goto UACPrompt
) else ( goto gotAdmin )

:UACPrompt
    echo Set UAC = CreateObject^("Shell.Application"^) > "%temp%\getadmin.vbs"
    echo UAC.ShellExecute "%~s0", "", "", "runas", 1 >> "%temp%\getadmin.vbs"

    "%temp%\getadmin.vbs"
    exit /B

:gotAdmin
    if exist "%temp%\getadmin.vbs" ( del "%temp%\getadmin.vbs" )
    pushd "%CD%"
    CD /D "%~dp0"
:: BatchGotAdmin (Run as Admin code ends)
:: Article: http://www.techgainer.com/create-batch-file-automatically-run-administrator

set process=king.exe
if not exist "%~dp0\%process%" (
	echo "No king.exe file found!"
	echo.
	echo "Closing in 5 seconds..."
	timeout /t 5
	exit
)

reg Query "HKLM\Hardware\Description\System\CentralProcessor\0" | find /i "x86" > NUL && set osArch=32BIT || set osArch=64BIT

echo "Activating Indeo 5 Video Codec..."
if %osArch%==64BIT (
	cmd /c "regsvr32 %SystemRoot%\SysWOW64\ir50_32.dll"
) else (
	cmd /c "regsvr32 %SystemRoot%\system32\ir50_32.dll"
)
cls
echo "Indeo 5 Video Codec activated!"
echo.
echo "Start King of the Road/Hard Truck 2..."
start %process%
cls
echo "King of the Road/Hard Truck 2 running!"
echo "Checking all 2 seconds if king.exe process is running..."

:checkIfProcessRuns
tasklist.exe | findstr "%process%" > 0
if errorlevel 1 (
	cls
	echo "No king.exe process running!"
	echo.
	echo "Deactivating Indeo 5 Video Codec..."
	if %osArch%==64BIT (
		cmd /c "regsvr32 /u %SystemRoot%\SysWOW64\ir50_32.dll"
	) else (
		cmd /c "regsvr32 /u %SystemRoot%\system32\ir50_32.dll"
	)
	cls
	echo "Indeo 5 Video Codec deactivated!"
	echo.
	echo "Closing in 3 seconds..."
	timeout /t 3 /nobreak > nul
	exit
) else (
	timeout /t 2 /nobreak > nul
	goto checkIfProcessRuns
)